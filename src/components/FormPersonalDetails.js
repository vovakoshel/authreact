import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

export class FormPersonalDetails extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };
  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { handleChange, styles } = this.props;
    const { password, login, confirmpassword } = this.props.values;

    const isInvalid =
      password !== confirmpassword || password === "" || login === "";

    return (
      <React.Fragment>
        <div className="container">
          <TextField
            required
            label="Login"
            onChange={handleChange("login")}
            defaultValue={login}
            margin="normal"
            style={styles.TextField}
          />
          <br />

          <br />
          <Button
            variant="contained"
            color="primary"
            onClick={this.continue}
            style={styles.Button}
            disabled={isInvalid}
          >
            Continue
          </Button>
          <Button
            variant="outlined"
            color="primary"
            onClick={this.back}
            style={styles.Button}
          >
            Back
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

const styles = {
  button: {
    margin: 15
  }
};

export default FormPersonalDetails;
