import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

export class FormUserDetails extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { values, handleChange, styles } = this.props;
    const {
      firstName,
      lastName,
      companyName,
      email,
      password,
      confirmpassword
    } = values;
    const isInvalid =
      firstName === "" ||
      lastName === "" ||
      companyName === "" ||
      email === "" ||
      password !== confirmpassword ||
      password === "";

    return (
      <React.Fragment>
        <div className="container">
          <TextField
            required
            label="First Name"
            onChange={handleChange("firstName")}
            defaultValue={firstName}
            margin="normal"
            style={styles.TextField}
          />
          <br />
          <TextField
            required
            label="Last Name"
            onChange={handleChange("lastName")}
            defaultValue={lastName}
            margin="normal"
            style={styles.TextField}
          />
          <br />
          <TextField
            required
            label="Company Name"
            onChange={handleChange("companyName")}
            defaultValue={companyName}
            margin="normal"
            style={styles.TextField}
          />
          <br />
          <TextField
            required
            label="Email"
            onChange={handleChange("email")}
            defaultValue={email}
            margin="normal"
            style={styles.TextField}
          />
          <br />
          <TextField
            required
            label="Password"
            type="password"
            onChange={handleChange("password")}
            defaultValue={password}
            margin="normal"
            style={styles.TextField}
          />
          <br />
          <TextField
            required
            label="Confirm Password"
            type="password"
            onChange={handleChange("confirmpassword")}
            defaultValue={confirmpassword}
            margin="normal"
            style={styles.TextField}
          />
          <Button
            disabled={isInvalid}
            variant="contained"
            color="primary"
            onClick={this.continue}
            style={styles.Button}
          >
            Continue
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default FormUserDetails;
