import React, { Component } from "react";

import { List, ListItem, ListItemText } from "@material-ui/core";
import Button from "@material-ui/core/Button";

export class Confirm extends Component {
  continue = e => {
    e.preventDefault();
    const {
      values: { firstName, lastName, email, companyName, password }
    } = this.props;
    //PROCESS FORM
    fetch("http://localhost:8080/Blog/Registration", {
      method: "POST",
      body: JSON.stringify({
        name: companyName,
        user: {
          name: firstName,
          surname: lastName,
          password: password,
          mail: email
        }
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const {
      values: { firstName, lastName, email, companyName, password },
      styles
    } = this.props;

    return (
      <React.Fragment>
        <div className="container">
          <List>
            <ListItem>
              <ListItemText primary={firstName} secondary="First Name" />
            </ListItem>
            <ListItem>
              <ListItemText primary={lastName} secondary="Last Name" />
            </ListItem>
            <ListItem>
              <ListItemText primary={companyName} secondary="Company Name" />
            </ListItem>
            <ListItem>
              <ListItemText primary={email} secondary="Email" />
            </ListItem>
            <ListItem>
              <ListItemText primary={password} secondary="Password" />
            </ListItem>
          </List>
          <br />

          <Button
            variant="contained"
            color="primary"
            onClick={this.continue}
            style={styles.Button}
          >
            Confirm & Continue
          </Button>
          <Button
            variant="outlined"
            color="primary"
            onClick={this.back}
            style={styles.Button}
          >
            Back
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default Confirm;
