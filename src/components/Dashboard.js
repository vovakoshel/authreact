import React, { Component } from "react";
export class Dashboard extends Component {
  state = {
    articles: []
  };

  fetchAllArticles = async () => {
    const articles = await fetch(`http://localhost:8080/Blog/ViewAllArticles`, {
      credentials: "include"
    }).then(response => response.json());
    return articles.reverse();
  };

  async componentDidMount() {
    const articles = await this.fetchAllArticles();
    this.setState({
      articles: articles
    });
  }

  render() {
    return <div>говно панель тут</div>;
  }
}

export default Dashboard;
