import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

export class Success extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <h2>Thank You For Your Submission</h2>

          <Button
            variant="contained"
            color="primary"
            component={Link}
            to="/SignIn"
          >
            Go to login form!
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default Success;
