import React, { Component } from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import blueGrey from "@material-ui/core/colors/blueGrey";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: blueGrey[800]
    }
  }
});

export class Navbar extends Component {
  render() {
    return (
      <div>
        <MuiThemeProvider theme={theme}>
          <AppBar position="static" color="primary">
            <Toolbar>
              <Typography variant="h5" color="inherit">
                CapitalLetter
              </Typography>

              <Button component={Link} to="/" color="inherit">
                Home
              </Button>
              <Button component={Link} to="/SignIn" color="inherit">
                Sign In
              </Button>
              <Button component={Link} to="/SignUp" color="inherit">
                Sign Up
              </Button>
            </Toolbar>
          </AppBar>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default Navbar;

// const MyLink = props => <Link to="/open-collective" {...props} />

// <Button component={MyLink}>
//   Link
// </Button>
