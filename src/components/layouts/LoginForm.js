import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const styles = {
  TextField: {
    width: 300
  }
};

export class LoginForm extends Component {
  state = {
    password: "",
    email: "",
    error: null
  };

  onClickLogin = e => {
    e.preventDefault();
    const { email, password } = this.state;
    fetch("http://localhost:8080/Blog/Authentication", {
      method: "POST",
      // mode: "same-origin", // no-cors, cors, *same-origin
      // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      // credentials: "same-origin", // include, *same-origin, omit
      body: JSON.stringify({
        mail: email,
        password: password
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
      // redirect: "follow", // manual, *follow, error
      // referrer: "no-referrer" // no-referrer, *client
    })
      .then(res => {
        console.log(res.headers.get("My-token"));
        if (res.status === 200) {
          this.props.history.push("/dashboard");
        } else {
          const error = new Error(res.error);
          throw error;
        }
      })
      .catch(err => {
        console.error(err);
        alert("Error logging in please try again");
      });
  };

  handleChange = input => e => {
    this.setState({
      [input]: e.target.value
    });
  };

  render() {
    const { email, password } = this.state;
    const isInvalid = email === "" || password === "";

    return (
      <div className="container">
        <TextField
          style={styles.TextField}
          label="Email"
          required
          onChange={this.handleChange("email")}
          margin="normal"
        />
        <br />
        <TextField
          style={styles.TextField}
          type="password"
          label="Password"
          required
          onChange={this.handleChange("password")}
          margin="normal"
        />
        <br />
        <Button
          disabled={isInvalid}
          variant="contained"
          color="primary"
          onClick={this.onClickLogin}
          component={Link}
          to="/"
        >
          Login
        </Button>
      </div>
    );
  }
}

export default LoginForm;
