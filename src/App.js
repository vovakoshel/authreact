import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { UserForm } from "./components/UserForm";
import { LoginForm } from "./components/layouts/LoginForm";
import Home from "./components/pages/Home";
import Dashboard from "./components/Dashboard";

import Navbar from "./components/pages/Navbar";
import CssBaseline from "@material-ui/core/CssBaseline";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <CssBaseline />
          <Navbar />
          <Route path="/" exact={true} component={Home} />
          <Route path="/SignUp" exact={true} component={UserForm} />
          <Route path="/SignIn" exact={true} component={LoginForm} />
          <Route path="/Dashboard" exact={true} component={Dashboard} />
        </div>
      </Router>
    );
  }
}

export default App;
